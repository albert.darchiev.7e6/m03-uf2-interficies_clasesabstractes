//AUTONOMOUS CAR PROTOTYPE

interface CarSensors{
    fun isThereSomethingAt(direction: Direction) : Boolean
    fun go(direction : Direction)
    fun stop()
}

enum class Direction(){
    FRONT, LEFT, RIGHT
}
class AutonomousCar : CarSensors{
    fun doNextNSteps(n :Int){
        for (i in 0 ..n) {

        if (!isThereSomethingAt(Direction.FRONT)) go(Direction.FRONT)
        else {
            if (!isThereSomethingAt(Direction.RIGHT)) go(Direction.RIGHT)
            else {
                if (!isThereSomethingAt(Direction.LEFT)) go(Direction.LEFT)
                else stop()
                }
            }
        }
    }

    override fun isThereSomethingAt(direction: Direction): Boolean {
        TODO("CHECK DIRECTION")
    }
    override fun go(direction: Direction) {
        TODO("CHANGE DIRECTION")
    }
    override fun stop() {
        TODO("STOP")
    }
}

fun main(){
    AutonomousCar().doNextNSteps(10)
}