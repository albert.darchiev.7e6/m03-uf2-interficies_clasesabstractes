import java.util.*

//GYM CONTROL APP

interface GymControlReader{
    fun nextId() : String
}

class GymControlManualReader(val scanner: Scanner = Scanner(System.`in`)) : GymControlReader {
    override fun nextId() = scanner.next()

    fun readInput(): MutableList<String>{
        val idList = mutableListOf<String>()
        for (i in 0 until 8){
            val id = nextId()
            if ("$id Entrada" !in idList) idList.add("$id Entrada")
            else idList.add("$id Sortida")
    }
        return idList
    }
}

fun main(){
    val idOutput = GymControlManualReader().readInput()
    idOutput.forEach { println(it) }
    }