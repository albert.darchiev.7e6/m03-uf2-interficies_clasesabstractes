//STUDENT TEXT GRADE
data class Student(val name: String, val textGrade: GRADES)
    enum class GRADES(grade: String) {
        SUSPES("suspés"),
        APROVAT("aprovat"),
        BE("bé"),
        NOTABLE("notable"),
        EXCELENT("excelent")
    }

fun main() {
    println(Student("John", GRADES.EXCELENT))
    println(Student("Eduard", GRADES.NOTABLE ))
}