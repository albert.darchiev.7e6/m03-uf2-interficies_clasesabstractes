import java.util.Scanner
//RAINBOW

//Rojo, naranja, amarillo, verde, añil, azul y violeta
enum class COLORS() {
    VERMELL,
    TARONGE,
    GROC,
    VERD,
    ANYIL,
    BLAU,
    VIOLETA
}

fun main(){
    val scanner = Scanner(System.`in`)
    val colorSCAN = scanner.next().uppercase()
    var exists = false
    for (i in COLORS.values()){
        if (colorSCAN == i.name) exists = true
    }
    println(exists)
}