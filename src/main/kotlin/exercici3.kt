//INSTRUMENT SIMULATOR
abstract class Instrument(){
    abstract fun makeSounds(times: Int)
}
class Triangle(val a: Int): Instrument(){
    override fun makeSounds(times: Int) {
        repeat(times){
            print("T")
            repeat(a){ print("I")}
            println("NC")
        }
    }
}
class Drump(val a: String):Instrument(){
    override fun makeSounds(times: Int){
        repeat(times){
        when(a){
            "A"-> println("TAAAM")
            "O"-> println("TOOOM")
            "U"-> println("TUUUM")
        }
    }
}
}

fun main() {
    val instruments: List<Instrument> = listOf(
        Triangle(5),
        Drump("A"),
        Drump("O"),
        Triangle(1),
        Triangle(5)
    )
    play(instruments)
}
private fun play(instruments: List<Instrument>) {
    for (instrument in instruments) {
        instrument.makeSounds(2)
    }
}